// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from 'firebase/auth'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAK3pF_GI_qTk26YPdFDiNJFEjbxKXm7oM",
  authDomain: "binar-ch5-6f770.firebaseapp.com",
  projectId: "binar-ch5-6f770",
  storageBucket: "binar-ch5-6f770.appspot.com",
  messagingSenderId: "818037144149",
  appId: "1:818037144149:web:ff6c3c373a86e9ffb2f2dc"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export default app