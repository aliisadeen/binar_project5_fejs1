import { Alert, Box, Button, TextField, Typography } from '@mui/material'
import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { useUserAuth } from '../context/UserAuthContext'
import GoogleButton from 'react-google-button'

const Sign = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const {signUp, googleSignIn} = useUserAuth()
    const [error, setError] = useState('')
    const navigate = useNavigate()
    const googleButtonStyle = { width: '100%' };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setError('')
        console.log(email, password)
        try {
            await signUp(email, password).then((userCredential) => {
                const userEmail = userCredential.user.email;
                const save = localStorage.setItem('email',  userEmail);
                console.log(localStorage)
                navigate('/',{state:{notif: `${userEmail} successfully resgitered.` }})
            })
        } catch (err) {
            setError(err.message)
        }
    }

    const handleGoogleSignIn = async (e) => {
        e.preventDefault();
        try {
        await googleSignIn().then((userCredential) => {
            const userEmail = userCredential.user.email;
            const save = localStorage.setItem('email',  userEmail);
            console.log(localStorage)
            navigate("/landing");
        });
        } catch (error) {
        console.log(error.message);
        }
    };

    return (
        <Box sx={{background:'white', display:'grid', gap:2, p:2}}>
            <Typography sx={{justifySelf:'center'}}>Sign up</Typography>

            {error && <Alert severity="error">{error}</Alert>}

            <TextField id='outlined-basic' label='Email' variant='outlined' onChange={(e) => setEmail(e.target.value)}/>
            <TextField id='outlined-basic' label='Password' variant='outlined' type='password' onChange={(e) => setPassword(e.target.value)}/>

            <Button color='secondary' variant='contained' onClick={handleSubmit} sx={{height:'50px'}}>Sign up</Button>
            <GoogleButton id='google' onClick={handleGoogleSignIn} style={googleButtonStyle}/>

            <Typography sx={{justifySelf:'center'}}>Already have an account? <Link to='/'>Login</Link></Typography>
        </Box>
    )
}

export default Sign