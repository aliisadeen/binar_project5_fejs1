import React, { useState } from 'react'
import { Alert, Box, Button, TextField, Typography } from '@mui/material'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import { useUserAuth } from '../context/UserAuthContext'
import GoogleButton from 'react-google-button'

const Login = () => {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [error, setError] = useState("")
    // const [success, setSuccess] = useState('')
    const navigate = useNavigate()
    const {logIn, googleSignIn}= useUserAuth()
    const googleButtonStyle = { width: '100%' };
    const location = useLocation();
    const success = location.state ? location.state.notif : null;

    const handleSubmit = async(e) => {
        e.preventDefault()
        setError("")
        console.log(email, password)
        try{
        await logIn(email, password).then((userCredential) => {
            const userEmail = userCredential.user.email;
            const save = localStorage.setItem('email',  userEmail);
            console.log(localStorage)
            navigate("/landing");
        })
        }catch(err){
        setError(err.message)
        }
    }

    const handleGoogleSignIn = async (e) => {
        e.preventDefault();
        try {
        await googleSignIn().then((userCredential) => {
            const userEmail = userCredential.user.email;
            const save = localStorage.setItem('email',  userEmail);
            console.log(localStorage)
            navigate("/landing");
        })
        } catch (error) {
        console.log(error.message);
        }
    };

    return (
        <>
            <Box sx={{background:'white', display:'grid', gap:2, p:2}}>
                <Typography sx={{justifySelf:'center'}}>Login</Typography>

                {error && <Alert severity="error">{error}</Alert>}
                {success && !error &&  <Alert severity="success">{success}</Alert>}

                <TextField id='outlined-basic' label='Email' variant='outlined' onChange={(e) => setEmail(e.target.value)}/>
                <TextField id='outlined-basic' label='Password' variant='outlined' type='password' onChange={(e) => setPassword(e.target.value)}/>

                <Button color='secondary' variant='contained' onClick={handleSubmit} sx={{height:'50px'}}>Login</Button>
                <GoogleButton id='google' onClick={handleGoogleSignIn} style={googleButtonStyle}/>

                <Typography sx={{justifySelf:'center'}}>Don't have an account? <Link to='/signup'>Sign up</Link></Typography>
            </Box>
        </>
    )
}

export default Login