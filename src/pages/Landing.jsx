import React from 'react'
import Main from '../components/Main'
import Navbar from '../components/Navbar'

const Landing = () => {
    return (
        <>
            <Navbar/>
            <Main/>
        </>
    )
}

export default Landing