import React from 'react'
import Movies from '../components/Movies'
import Navbar from '../components/Navbar'

const Other = () => {
  return (
    <>
      <Navbar/>
      <Movies/>
    </>
    
  )
}

export default Other