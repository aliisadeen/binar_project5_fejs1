import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Detail from './components/Detail';
import Landing from './pages/Landing';
import Other from './pages/Other';
import Search from './components/Search';
import Login from './components/Login';
import Sign from './components/Sign';
import { UserAuthContextProvider } from './context/UserAuthContext';

function App() {
  return (
    <BrowserRouter>
      <UserAuthContextProvider>
        <Routes>
          <Route path='/' element={<Login/>}/>
          <Route path='/signup' element={<Sign/>}/>
          <Route path='/landing' element={<Landing/>}/>
          <Route path='/movies' element={<Other/>}/>
          <Route path='/detail' element={<Detail />} />
          <Route path='/search' element={<Search />} />
        </Routes>
      </UserAuthContextProvider>
      
    </BrowserRouter>
  );
}

export default App;
